import React, { Component } from 'react';
import Navbar from './Navbar';
import {Switch, Route} from 'react-router-dom';
import Home from './Home';
import About from './About';
import Contact from './Contact';

class Main extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
                <Navbar/>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/contact" component={Contact} />
                </Switch>

                {/* <Services/> */}
                {/* <Slider/> */}
                {/* <Footer/> */}
            </div>
         );
    }
}
 
export default Main;